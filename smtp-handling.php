<?php
/*
Plugin Name:  Mandatory plugins
Plugin URI:   https://jumpgroup.it/
Description:  Adds Configs for Image Handling
Version:      1.1.0
Author:       Jump Group
License:      MIT License
*/

namespace JumpGroup\Mandatory;
use JumpGroup\Mandatory\AddMailFrom;
use JumpGroup\Mandatory\SMTPConfig;
use JumpGroup\Mandatory\ForceLogin;
use JumpGroup\Mandatory\RegisterJumpAsAdmin;

if ( ! defined( 'WPINC' ) ) {
	die;
}

class Init{

  protected static $instance;

    public static function get_instance(){
      if( null == self::$instance ){
        self::$instance = new self();
      }
      return self::$instance;
    }

    protected function __construct(){
      AddMailFrom::init();
      SMTPConfig::init();

      if ( env('WP_ENV') == 'staging' ){
        ForceLogin::init();
        RegisterJumpAsAdmin::init();
      }
    }
}

$instance = Init::get_instance();
