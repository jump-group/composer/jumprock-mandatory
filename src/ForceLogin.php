<?php

namespace JumpGroup\Mandatory;

class ForceLogin{
    public static function init() {

        add_action( 'template_redirect', function () {
            if ( !is_user_logged_in() ) {
                auth_redirect();
            }
        });
    }
}