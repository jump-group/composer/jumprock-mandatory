<?php

namespace JumpGroup\Mandatory;

class RegisterJumpAsAdmin{

    public static function init() {

        add_action( 'rtcamp.google_user_created', function ($uid, $user) {
            $u = new \WP_User( $uid );
    
            // Remove role
            $u->remove_role( 'subscriber' );

            // Add role
            $u->add_role( 'administrator' );
        }, 10, 2);

    }
}