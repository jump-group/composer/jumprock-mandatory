<?php

namespace JumpGroup\Mandatory;

class SMTPConfig{
    public static function init() {

        if ( env( 'WP_MAIL_SMTP_AUTH' ) ) {
            add_action( 'phpmailer_init', function ( $phpmailer ) {
                $phpmailer->isSMTP();
                $phpmailer->SMTPAuth   = true;
                $phpmailer->Username   = env( 'WP_MAIL_SMTP_USERNAME' );
                $phpmailer->Password   = env( 'WP_MAIL_SMTP_PASSWORD' );
                $phpmailer->Host       = env( 'WP_MAIL_SMTP_HOST' );
                $phpmailer->Port       = env( 'WP_MAIL_SMTP_PORT' );
                $phpmailer->SMTPSecure = env( 'WP_MAIL_SMTP_SECURE' );
            }, 999 );
        
        }else{
            add_action( 'phpmailer_init', function ( $phpmailer ) {
    
                $phpmailer->IsSMTP();
                if(env( 'WP_MAIL_SMTP_HOST' ) != null){
                    $phpmailer->Host       = env( 'WP_MAIL_SMTP_HOST' );
                }else{
                    $phpmailer->Host = 'mailhog';
                }
                $phpmailer->Port = 1025;
                $phpmailer->SMTPAuth = false;

            }, 999 );
        }
    }
}