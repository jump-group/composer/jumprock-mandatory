<?php

namespace JumpGroup\Mandatory;

class AddMailFrom{
    
    public static function init() {
        if ( env( 'WP_MAIL_FROM' ) ) {

            add_filter( 'wp_mail_from', function () {
                return env( 'WP_MAIL_FROM' );
            });

            add_filter( 'wp_mail_from_name', function () {
                return env( 'WP_MAIL_FROM_NAME' ) ? env( 'WP_MAIL_FROM_NAME' ) : get_bloginfo( 'name' );
            } );
        }
    }
}